import Tkinter
import board_data

class TicTacToeCanvas(Tkinter.Canvas):
    TILE_SPACING = 4

    def __init__(self, master, board_data):
        Tkinter.Canvas.__init__(self, master, width=200, height=200)
        self.pack()

        self._board_data = board_data

        self.update()

        self.bind("<Button-1>", self.on_lmb)
        self.lmb_callbacks = []

    # ------------------------------------------------------------------

    def on_lmb(self, evt):
        mx, my = evt.x, evt.y

        # determine which cell was clicked on; only trick here is that we
        #  don't count the first and last five pixels to prevent any
        #  [potential] misses on the edges of the frame and on the grid lines
        # it's not particularly likely, mind you
        cw, ch = self.tile_size

        # int division; doesn't matter since we want the integral part anyway
        cx, cy = mx / cw, my / ch

        is_in_zone = lambda val, bound: 5 < (val % bound) < bound-5

        if is_in_zone(mx, cw) and is_in_zone(my, ch):
            for callback in self.lmb_callbacks:
                callback(cx, cy)

    # ------------------------------------------------------------------

    def redraw(self):
        self.delete("all")

        self.redraw_grid()
        self.redraw_tiles()

    # ------------------------------------------------------------------

    @property
    def tile_size(self):
        return (self.winfo_width() / self._board_data.width,
                self.winfo_height() / self._board_data.height)

    def redraw_grid(self):
        """
        Draws tic-tac-toe grid and tiles onto self.
        """
        def draw_grid_line(x1, y1, x2, y2):
            self.create_line(x1, y1, x2, y2, fill="black", width=2)

        tile_w, tile_h = self.tile_size

        # draws vertical lines
        for x in xrange(1, self._board_data.width):
            draw_x = tile_w * x
            draw_grid_line(draw_x, 0, draw_x, self.winfo_height())

        # horizontal lines
        for y in xrange(1, self._board_data.height):
            draw_y = tile_h * y
            draw_grid_line(0, draw_y, self.winfo_width(), draw_y)

    def redraw_tiles(self):
        tile_w, tile_h = self.tile_size

        def draw_x(x, y, w, h, col):
            x += TicTacToeCanvas.TILE_SPACING
            y += TicTacToeCanvas.TILE_SPACING
            w -= TicTacToeCanvas.TILE_SPACING*2
            h -= TicTacToeCanvas.TILE_SPACING*2

            self.create_line(x, y, x+w, y+h, fill=col, width=4)
            self.create_line(x+w, y, x, y+h, fill=col, width=4)

        def draw_o(x, y, w, h, col):
            x += TicTacToeCanvas.TILE_SPACING
            y += TicTacToeCanvas.TILE_SPACING
            w -= TicTacToeCanvas.TILE_SPACING*2
            h -= TicTacToeCanvas.TILE_SPACING*2

            self.create_oval(x, y, x+w, y+h, outline=col, width=4)

        for y, row in enumerate(self._board_data.tile_status):
            for x, tile in enumerate(row):
                tile_x, tile_y = tile_w * x, tile_h * y

                if tile.name == "X":
                    draw_x(tile_x, tile_y, tile_w, tile_h, tile.color)
                elif tile.name == "O":
                    draw_o(tile_x, tile_y, tile_w, tile_h, tile.color)



class Scoreboard(Tkinter.PanedWindow):
    def __init__(self, players, scores, width):
        Tkinter.PanedWindow.__init__(self, orient=Tkinter.HORIZONTAL, width=width)

        self.pack()
        self.update()

    # ------------------------------------------------------------------

    def set_data(self, players, scores):
        self.players = players
        self.scores = scores

        self.remake_widgets()

    # ------------------------------------------------------------------

    def redraw(self):
        for player, score, entry in \
                zip(self.players, self.scores, self.score_entries):
            entry.delete("all")

            entry.create_text(
                entry.winfo_width() / 2, 10,
                text=str(score), fill=player.type.color)

    # ------------------------------------------------------------------

    def remake_widgets(self):
        for child in self.winfo_children():
            child.destroy()

        n = len(self.players)
        ind_width = self.winfo_width() / n

        def new_entry(name, color, x, y):
            entry = Tkinter.Canvas(self, width=ind_width, height=20)
            entry.grid(column=x, row=y)
            entry.update()

            entry.delete("all")
            entry.create_text(
                entry.winfo_width() / 2, 10, text=name, fill=color)

            return entry

        self.player_entries = [new_entry(player.name, player.type.color, i, 0)
                               for i,player in enumerate(self.players)]
        self.score_entries = [new_entry("0", player.type.color, i, 1)
                              for i, player in enumerate(self.players)]

        self.redraw()

