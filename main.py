"""
tic tac toe

Check the README file for more details, or the LICENSE file for the
license. It's MIT license, by the way.
"""

import Tkinter
import itertools
from ScrolledText import ScrolledText as TkScrolledText

import tkwidgets
import board_data
import game
import players

class TicTacToeApplication(Tkinter.Frame):
    # how many times we process tic-tac-toe related stuff; our stuff isn't
    #  exactly intensive, so 144Hz is probably safe for people with
    #  AWESOME PRO GAMING MONITORS
    TK_UPDATE_FPS = 144
    WIDTH = 600

    def __init__(self, master=None):
        # Tkinter.Frame is apparently declared as an old-style class, at least
        #  for the version I tested with; hence not using super
        Tkinter.Frame.__init__(self, master)
        self.pack()

        # main splitter; divides the top half (screen and log) from the
        #  bottom half (scores and rematch)
        self.vert_splitter = Tkinter.PanedWindow(
            orient=Tkinter.VERTICAL, width=TicTacToeApplication.WIDTH, height=280)
        self.vert_splitter.pack(fill=Tkinter.BOTH, expand=True)

        # TOP HALF: GAME AND LOG
        self.splitter = Tkinter.PanedWindow(
            orient=Tkinter.HORIZONTAL, width=TicTacToeApplication.WIDTH)
        self.splitter.pack(fill="x", expand=False)
        self.vert_splitter.add(self.splitter)

        # left side
        self.canvas = tkwidgets.TicTacToeCanvas(self.splitter, None)
        self.canvas.lmb_callbacks.append(self.cell_click)
        self.splitter.add(self.canvas)

        # right side
        self.log = TkScrolledText(self.splitter, height=8)
        self.log.pack(expand=True)
        self.splitter.add(self.log)


        # BOTTOM HALF: SCOREBOARD
        self.score_splitter = Tkinter.PanedWindow(
            orient=Tkinter.HORIZONTAL, width=TicTacToeApplication.WIDTH)
        self.score_splitter.pack(fill=Tkinter.BOTH)
        self.vert_splitter.add(self.score_splitter)

        self.scoreboard = tkwidgets.Scoreboard(None, None, 500)
        self.scoreboard.pack()
        self.score_splitter.add(self.scoreboard)

        self.rematch_button = Tkinter.Button(
            self.score_splitter, text="Rematch", command=self.rematch, state="disabled")
        self.rematch_button.pack()
        self.score_splitter.add(self.rematch_button)

        self.new_game()
        self.after(1000 / TicTacToeApplication.TK_UPDATE_FPS, self.event_loop)

    # ------------------------------------------------------------------

    def event_loop(self):
        self.canvas.redraw()

        self.game.update()

        self.after(1000 / TicTacToeApplication.TK_UPDATE_FPS, self.event_loop)

    # ------------------------------------------------------------------

    def new_game(self):
        self.game = game.TicTacToeGame([
            players.HumanPlayer(board_data.TileTypes.X),
            players.RandomBot(board_data.TileTypes.O)
        ])
        self.game.on_make_move = self.make_move
        self.game.on_game_end = self.game_end

        self.canvas._board_data = self.game.board_data
        self.canvas.redraw()

        self.scoreboard.set_data(self.game.players, self.game.scores)
        self.scoreboard.redraw()

    def rematch(self):
        self.rematch_button["state"] = "disabled"
        self.game.rematch()

    def make_move(self, x, y, player):
        str = "%s plays %s in (%d, %d)\n" % \
              (player.name, player.type.name, x+1, y+1)

        self.log.insert(Tkinter.INSERT, str)
        self.log.see(Tkinter.END)

    # ------------------------------------------------------------------

    def game_end(self, winning_player, end_type):
        name = winning_player.name
        if end_type is game.GameResults.VICTORY:
            self.log.insert(Tkinter.INSERT, "%s wins!\n" % name)
        elif end_type is game.GameResults.TIE:
            self.log.insert(Tkinter.INSERT, "It's a tie!\n")
        elif end_type is game.GameResults.FORFEIT_AI:
            self.log.insert(Tkinter.INSERT,
                            "%s forfeited due to an AI problem\n" % name)
        self.log.see(Tkinter.END)

        self.scoreboard.redraw()
        self.rematch_button["state"] = "normal"

    def cell_click(self, cell_x, cell_y):
        self.game.player_input(cell_x, cell_y)
        self.canvas.redraw()


root = Tkinter.Tk()
root.wm_title("Tic-Tac-Toe")
app = TicTacToeApplication(master=root)
app.mainloop()