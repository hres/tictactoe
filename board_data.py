import itertools

class TileTypes(object):
    class TileType(object):
        def __init__(self, name, color):
            self.name = name
            self.color = color

    EMPTY = TileType("", "white")
    X = TileType("X", "black")
    O = TileType("O", "red")
    # amazing special bonus tile types!
    SPECIAL_O = TileType("O", "green")
    SPECIAL_X = TileType("X", "magenta")

class BoardData(object):
    def __init__(self):
        self.width = 3
        self.height = 3

        self.clear_board()

    # ------------------------------------------------------------------

    def clear_board(self):
        # store as [y][x]; i.e. row major in case want to migrate to numpy
        #  or something (which is very unlikely, but hey)
        # large listcomp is needed vs. array multiplication to prevent
        #  copying a reference to a mutable list
        self.tile_status = [
            [
                TileTypes.EMPTY
                for _ in xrange(self.width)
            ]
            for _ in xrange(self.height)
        ]

    def claim_tile(self, x, y, tile_type):
        if self.tile_status[y][x] is not TileTypes.EMPTY:
            return False

        self.tile_status[y][x] = tile_type

        return True

    # ------------------------------------------------------------------

    def is_board_full(self):
        return all(tile is not TileTypes.EMPTY
                   for row in self.tile_status
                   for tile in row)


    def empty_tiles(self):
        return [
            (x, y)
            for y, row in enumerate(self.tile_status)
            for x, tile in enumerate(row)
            if tile is TileTypes.EMPTY
        ]

    # ------------------------------------------------------------------

    def check_victory(self):
        rows = iter(self.tile_status)
        cols = iter(itertools.izip(*self.tile_status))

        # diagonals are awkward since we handle arbitrary rectangular sizes
        def get_diagonal(start_x, start_y, x_step):
            x,y = start_x, start_y
            while 0 <= x < self.width and 0 <= y < self.height:
                yield self.tile_status[y][x]
                x += x_step
                y += 1

        diags = [
            (get_diagonal(x,y,1), get_diagonal(x,y,-1))
            for x in xrange(self.width)
            for y in xrange(self.height)
        ]
        # we need to unpack diags since it's a list of tuples, not a flat
        #  list of iterables as we'd desire
        all_checks = itertools.chain(rows, cols, *diags)

        def check_for_three(iter):
            grouped_iter = itertools.groupby(iter)

            for key, group in grouped_iter:
                if len(list(group)) >= 3 and key is not TileTypes.EMPTY:
                    return key

            return False

        # thoroughly abuse next's sentinel value feature; filter through
        #  looking for a true element; if we can't find anything we search
        #  an empty iterable, and so next returns its sentinel value
        return next(itertools.ifilter(bool,
                                      (check_for_three(seq_iter)
                                       for seq_iter in all_checks)),
                    None)

