class Player(object):
    def __init__(self, name, tile_type):
        # whether or not you're X, or O
        self.type = tile_type
        self.name = name

        self.is_robot = False
        self.is_human = False

class HumanPlayer(Player):
    def __init__(self, tile_type, number=None):
        super(HumanPlayer, self).__init__("Human", tile_type)

        self.is_human = True

class RobotPlayer(Player):
    def __init__(self, name, tile_type):
        super(RobotPlayer, self).__init__(name, tile_type)

        self.is_robot = True

    def make_move(self, board_status):
        """
        Returns a (cx, cy).
        """
        raise NotImplementedError(
            "You must subclass RobotPlayer and implement make_move !")

class RandomBot(RobotPlayer):
    def __init__(self, tile_type):
        super(RandomBot, self).__init__("Monty \"Monte\" Carlo", tile_type)

    def make_move(self, board_status):
        import random

        moves = board_status.empty_tiles()
        decision = random.choice(moves)

        return decision
