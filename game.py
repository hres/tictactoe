import itertools

import board_data
import players

class GameStatuses(object):
    THINKING = object()
    TURN_DONE = object()
    GAME_DONE = object()

class GameResults(object):
    VICTORY = object()
    TIE = object()
    FORFEIT_AI = object()


class TicTacToeGame(object):
    def __init__(self, players):
        self.board_data = board_data.BoardData()

        self.players = players
        self.scores = [0 for _ in players]

        # not technically accurate
        self.rematch()

        # hooks to let the world outside know what's happening
        # on_make_move(move_x, move_y, player)
        self.on_make_move = None
        # on_game_end(player, end_type)
        self.on_game_end = None

    # ------------------------------------------------------------------

    def rematch(self):
        self.board_data.clear_board()

        self.player_iterator = itertools.cycle(self.players)
        self.game_status = GameStatuses.TURN_DONE
        self.advance_turn()

    # ------------------------------------------------------------------

    def forfeit_due_to_stupid_ai(self, forfeiter):
        self.end_game(forfeiter, result=GameResults.FORFEIT_AI)

    def end_game(self, player, result):
        self.game_status = GameStatuses.GAME_DONE

        if result is GameResults.VICTORY:
            # hacky method to increment score
            i = next(n for n,p in enumerate(self.players)
                     if p is player)
            self.scores[i] += 1

        if self.on_game_end:
            self.on_game_end(player, result)

    # ------------------------------------------------------------------

    def advance_turn(self):
        self.current_player = next(self.player_iterator)
        self.game_status = GameStatuses.THINKING

    def conclude_turn(self):
        winner = self.board_data.check_victory()

        if winner:
            # get the winning player; we only got the winning tile type
            # note that this assumes each tile type is associated with
            #  exactly one player; this isn't actually enforced anywhere...
            winning_player = next(player for player in self.players
                                  if player.type is winner)

            self.end_game(winning_player, result=GameResults.VICTORY)
            return

        # board is full == tie (we ruled out a victory earlier)
        tied = self.board_data.is_board_full()

        if tied:
            self.end_game(self.current_player, result=GameResults.TIE)
            return

        self.game_status = GameStatuses.TURN_DONE

    # ------------------------------------------------------------------

    def try_move(self, x, y, player):
        result = self.board_data.claim_tile(x, y, player.type)

        if result and self.on_make_move:
            self.on_make_move(x, y, player)

        return result

    def think(self):
        if self.current_player.is_robot:
            attempts = 0

            while attempts < 100:
                cx, cy = self.current_player.make_move(self.board_data)

                if self.try_move(cx, cy, self.current_player):
                    break

                attempts += 1
            else:
                self.forfeit_due_to_stupid_ai(self.current_player)

            self.conclude_turn()

        elif self.current_player.is_human:
            # just wait for input
            pass

    def player_input(self, cell_x, cell_y):
        if self.game_status is not GameStatuses.THINKING:
            return

        # only go if it's a human's turn
        if self.current_player.is_human:
            success = self.try_move(cell_x, cell_y, self.current_player)

            if success:
                self.conclude_turn()

    # ------------------------------------------------------------------

    def update(self):
        if self.game_status is GameStatuses.THINKING:
            self.think()
        elif self.game_status is GameStatuses.TURN_DONE:
            self.advance_turn()
        elif self.game_status is GameStatuses.GAME_DONE:
            # we can't do anything at this point
            pass

